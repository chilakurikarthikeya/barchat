import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  @Input() height :string = "150px";
  @Input() width :string = "40px";
  @Input() backGroundColor: string = "#F7F9FA"
  @Input() fillColor :string = "#6B4EFF"
  @Input() fillPersentage: string = "0%";
  @Input() borderRadius: string = "10px";
  @Input() type :any= 2;
  outerStyle = `height: ${this.height}; width: ${this.width}; background-color: ${this.backGroundColor}; border-radius:${this.borderRadius}; `
  innerStyle = `height: ${this.fillPersentage}; width: ${this.width}; background-color: ${this.fillColor}; border-radius:${this.borderRadius}; `
  type2Styles = `height: ${this.height}; width: ${this.width}; background-color: ${this.backGroundColor}; border-radius:${this.borderRadius};`


  barchartType2DataPoints = {
    x_labels:[0,20,40,60,80,100],
    y_label_color:'#6B4EFF',
    y_labels:['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
    x_label_color:'#6B4EFF',
    data:[
      {label:'Sunday',values:[{value:100,color:'#6B4EFF'}]},
      {label:'Monday',values:[{value:0,color:'#6B4EFF'}]},
      {label:'Tuesday',values:[{value:20,color:'#6B4EFF'}]},
      {label:'Wednesday',values:[{value:60,color:'#6B4EFF'}]},
      {label:'Thursday',values:[{value:80,color:'#6B4EFF'}]},
      {label:'Friday',values:[{value:50,color:'#6B4EFF'}]},
      {label:'Saturday',values:[{value:20,color:'#6B4EFF'}]},
    ]
  }
  
  constructor() { }

  ngOnInit(): void {
    }
}
